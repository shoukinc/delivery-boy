package com.b2infosoft.svyh.sudhmilkdelivery.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;


public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
          imageView = findViewById(R.id.image_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (StorePrefs.getDefaults("LogedIn",SplashActivity.this) != null) {

                        Intent intent1 = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent1);
                    }
                    else{
                        Intent intent2 = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent2);
                    }
                finish();
            }
        }, SPLASH_TIME_OUT);


    }


}
