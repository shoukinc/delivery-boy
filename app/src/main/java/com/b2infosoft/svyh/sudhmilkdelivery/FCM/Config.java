package com.b2infosoft.svyh.sudhmilkdelivery.FCM;

/**
 * Created by Choudhary on 19-Apr-19.
 */

public class Config {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String LOGIN_COMPLETE = "loginComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = " sudhMilk delivery firebase";
    public static final String FCMCHANNEL_ID = "01";
    public static final String FCMCHANNEL_NAME = "sudhMilk Delivery";
    public static final String FCMCHANNEL_DESCRIPTION = "Sudh Milk Description";
}
