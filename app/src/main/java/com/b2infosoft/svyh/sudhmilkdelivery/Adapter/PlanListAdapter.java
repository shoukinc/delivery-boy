package com.b2infosoft.svyh.sudhmilkdelivery.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.b2infosoft.svyh.sudhmilkdelivery.Interface.CalculatePriceListener;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUploadPlanList;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUserPlanList;
import com.b2infosoft.svyh.sudhmilkdelivery.R;

import java.util.ArrayList;
import java.util.List;



public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.MyViewHolder>
{
    List<BeanUserPlanList> userPlanLists;
    Context mContext;
    Integer price = 0;
    Integer planTotalPrice = 0,grossPrice = 0,grossWeight = 0, qtncount=0;
    Integer plan_total_price=0;
    CalculatePriceListener listener;
    List<BeanUploadPlanList> uploadPlanList;
    int return_bottle=0;
    int bottlecount = 0;
    String strReturnBottle="0";

    public PlanListAdapter(Context mContext,  List<BeanUserPlanList> userPlanLists,CalculatePriceListener listener)
    {
      this.mContext = mContext;
      this.userPlanLists=userPlanLists;
      this.listener=listener;
        uploadPlanList = new ArrayList<>();
    }

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.plan_list_row_item, viewGroup, false);
        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {

        final BeanUserPlanList userAlbum = userPlanLists.get(position);

        holder.tv_product_name.setText(userAlbum.getProduct_name());
        holder. tvQnItemCount.setText(userAlbum.getWeight());
        holder. tvBottleCount.setText(strReturnBottle);

        price = userAlbum.getPrice();
        String str_weight = userAlbum.getWeight();
        String[] spitStr= str_weight.split("\\|");
        String number= spitStr[0].replaceAll("[^0-9]", "");
        Integer weight = Integer.valueOf(number);

        planTotalPrice = weight * price;
         qtncount = weight;

        grossPrice=grossPrice+planTotalPrice;
        grossWeight=grossWeight+weight;
        System.out.println("==planTotalPrice===="+planTotalPrice);
        holder.tv_product_price.setText(mContext.getString(R.string.Rupee_symbol)+" "+Integer.toString(planTotalPrice));
        return_bottle=0;


        if (position==userPlanLists.size()-1)
        {
            System.out.println("==planTotalPrice===="+grossPrice);

            listener.CalculatetotalPrice(grossPrice,grossWeight, uploadPlanList,return_bottle);
        }
        holder. btn_Qtn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price = userAlbum.getPrice();
                qtncount= Integer.valueOf(holder.tvQnItemCount.getText().toString());

                QtyIncreaseInteger(position);
            }

            private void QtyIncreaseInteger(int position) {
                qtncount = qtncount+1;

                grossWeight=grossWeight+1;
                grossPrice=grossPrice+price;
                plan_total_price=qtncount*price;
                String weight = String.valueOf(qtncount);
                uploadPlanList.get(position).setWeight(weight);

                System.out.println("==weight=="+weight);
                String qtn_value = Integer.toString(Integer.parseInt(weight));
                holder.tvQnItemCount.setText(qtn_value);

                System.out.println("==totalPrice==="+price);
                String str_totalPrice = Integer.toString(price);
                holder.tv_product_price.setText(mContext.getString(R.string.Rupee_symbol)+" "+str_totalPrice);

                listener.CalculatetotalPrice(grossPrice,grossWeight,uploadPlanList,return_bottle);

            }
        });
        holder. btn_Qtn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price = userAlbum.getPrice();
                qtncount= Integer.valueOf(holder.tvQnItemCount.getText().toString());
                QtyDecreaseInteger(position);
            }

            private void QtyDecreaseInteger(int position)
            {
                if(qtncount >0)
                {
                    qtncount = qtncount - 1;
                    plan_total_price = qtncount * price;
                    grossWeight=grossWeight-1;
                    grossPrice=grossPrice-price;
                    String weight = String.valueOf(qtncount);
                    uploadPlanList.get(position).setWeight(weight);

                    System.out.println("==weight=="+weight);
                    String qtn_value = Integer.toString(Integer.parseInt(weight));
                    holder.tvQnItemCount.setText(qtn_value);

                    System.out.println("==totalPrice==="+price);
                    String str_totalPrice = Integer.toString(price);
                    holder.tv_product_price.setText(mContext.getString(R.string.Rupee_symbol)+" "+str_totalPrice);


                    listener.CalculatetotalPrice(grossPrice,grossWeight,uploadPlanList,return_bottle);
                }
            }


        });
        holder. btn_bottle_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottlecount= Integer.parseInt(holder.tvBottleCount.getText().toString());

                bottlecount = bottlecount+ 1;
                return_bottle = return_bottle+1;
                 strReturnBottle = Integer.toString(bottlecount);
                holder. tvBottleCount.setText(strReturnBottle);
                uploadPlanList.get(position).returnBottle=bottlecount;
                listener.CalculatetotalPrice(grossPrice,grossWeight, uploadPlanList,return_bottle);
            }
        });
        holder.btn_bottle_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottlecount= Integer.parseInt(holder.tvBottleCount.getText().toString());
                if(bottlecount >0) {
                    bottlecount = bottlecount - 1;
                    return_bottle = return_bottle - 1;
                    uploadPlanList.get(position).returnBottle=bottlecount;

                    System.out.println("===return_bottle===="+return_bottle);
                    strReturnBottle = Integer.toString(bottlecount);
                    holder.tvBottleCount.setText(strReturnBottle);
                    listener.CalculatetotalPrice(grossPrice,grossWeight, uploadPlanList,return_bottle);

                }
            }
        });

        uploadPlanList.add(new BeanUploadPlanList(userAlbum.getId(),userAlbum.getProduct_name(),userAlbum.getWeight(),userAlbum.getPrice()));

    }

    @Override
    public int getItemCount()
    {
        return userPlanLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_product_name,tv_product_price,tvQnItemCount,tvBottleCount;
        ImageView btn_bottle_minus, btn_bottle_plus;
        ImageView btn_Qtn_minus,btn_Qtn_plus;


        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tv_product_name = itemView.findViewById(R.id.tvProductName);
            tv_product_price = itemView.findViewById(R.id.tvPrice);
            tvQnItemCount = itemView.findViewById(R.id.tvItemCount);
            btn_Qtn_minus = itemView.findViewById(R.id.imgQtyMinus);
            btn_Qtn_plus = itemView.findViewById(R.id.imgQtyPlus);
            tvBottleCount = itemView.findViewById(R.id.tvBottleCount);
            btn_bottle_minus = itemView.findViewById(R.id.imgBottleMinus);
            btn_bottle_plus = itemView.findViewById(R.id.imgBottlePlus);

        }

    }




}
