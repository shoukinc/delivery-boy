package com.b2infosoft.svyh.sudhmilkdelivery.Activity;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.b2infosoft.svyh.sudhmilkdelivery.Fragment.NotificationFragment;
import com.b2infosoft.svyh.sudhmilkdelivery.Fragment.ProfileFragment;
import com.b2infosoft.svyh.sudhmilkdelivery.Fragment.UserListFragment;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import static com.b2infosoft.svyh.sudhmilkdelivery.FCM.Config.FCMCHANNEL_DESCRIPTION;
import static com.b2infosoft.svyh.sudhmilkdelivery.FCM.Config.FCMCHANNEL_NAME;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    Fragment fragment;
    Intent intent;
    String selected_frag = "";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new UserListFragment();
                    fragmentset(fragment);
                    return true;
                case R.id.navigation_profile:
                    fragment = new ProfileFragment();
                    fragmentset(fragment);

                    return true;
                case R.id.navigation_notifications:
                    fragment = new NotificationFragment();
                    fragmentset(fragment);

                    return true;
            }
            return false;
        }
    };

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation =  findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fragment = new UserListFragment();
        fragmentset(fragment);
        intent = getIntent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(getString(R.string.notification_channel_id), FCMCHANNEL_NAME, importance);
            mChannel.setDescription(FCMCHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);

        }


        if (intent.getExtras() == null) {
            selected_frag = getString(R.string.title_home);
            fragment = new UserListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
        } else {
            selected_frag = getString(R.string.notification);
            Bundle bundle = new Bundle();
            bundle.putString("from", "dashboard");
            fragment = new NotificationFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
        }

    }
    private void fragmentset(Fragment fragment)
    {


        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(),SplashActivity.class);
        startActivity(intent);
        finish();
    }
}
