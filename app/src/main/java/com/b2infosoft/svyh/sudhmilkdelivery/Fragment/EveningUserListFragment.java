package com.b2infosoft.svyh.sudhmilkdelivery.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.b2infosoft.svyh.sudhmilkdelivery.Adapter.UserListApdater;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanExtraOrderDetail;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUserList;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUserPlanList;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.AppConstant;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;
import com.b2infosoft.svyh.sudhmilkdelivery.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.AppConstant.BaseImageUrl;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs.KEY_id;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.isNetworkAvaliable;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.resetNullString;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.showToast;

public class EveningUserListFragment extends Fragment {
    View view;
    Context mContext;
    RecyclerView recycle__userList;
    Toolbar toolbar;
    List<BeanExtraOrderDetail> extraOrderDetailsList;
    List<BeanUserList> userLists;
    SwipeRefreshLayout pullToRefresh;

    LinearLayout layout_search;
    UserListApdater userListApdater;
    List<BeanUserPlanList> userPlanLists ;
    EditText edtSearch;
    String query = "";
    Animation animation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_list, container, false);

        mContext = getActivity();
        userLists = new ArrayList<>();
        recycle__userList = view.findViewById(R.id.recycle__userList);
        layout_search = view.findViewById(R.id.layout_search);
        edtSearch = view.findViewById(R.id.edtSearch);

        pullToRefresh = view.findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            int Refreshcounter = 1;

            @Override
            public void onRefresh() {

                userListApdater.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });


        System.out.println("==fragment==evening=");
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence query, int arg1, int arg2,
                                      int arg3) {
                userListApdater.filterSearch(String.valueOf(query));
            }

        });
        getUserList();

        return view;
    }

    public void getUserList() {
        if (isNetworkAvaliable(mContext))
        {

            WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                @Override
                public void handleResponse(String response) {
                    try {
                        userLists=new ArrayList<>();
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            BaseImageUrl = jsonObject.getString("path");

                            JSONArray mainJsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < mainJsonArray.length(); i++)
                            { String milk_weight=""; Integer milk_price=0;
                                String pendingBottle="";

                                JSONObject object = mainJsonArray.getJSONObject(i);

                                if( object.getString("evening").equalsIgnoreCase("1"))
                                {

                                    if(object.getString("milk_weight").equalsIgnoreCase("null"))
                                    {
                                        milk_weight="";
                                    }else {
                                        // milk_weight = Integer.valueOf(getIntegerValue(object.getString("milk_weight")));
                                        milk_weight= object.getString("milk_weight");
                                    }
                                    if(object.getString("total_return_bottal").equalsIgnoreCase("null"))
                                    {
                                        pendingBottle="0";
                                    }else {

                                        pendingBottle= object.getString("total_return_bottal");
                                    }
                                    if(object.getString("milk_price").equalsIgnoreCase("null"))
                                    {
                                        milk_price=0;
                                    }else {
                                        milk_price= object.getInt("milk_price");
                                    }

                                    JSONArray orderJSonArray = object.getJSONArray("order_details");
                                    extraOrderDetailsList = new ArrayList<>();


                                    for(int j=0;j<orderJSonArray.length();j++)
                                    {
                                        JSONObject orderObject = orderJSonArray.getJSONObject(j);
                                        {
                                            extraOrderDetailsList.add(new BeanExtraOrderDetail(
                                                    resetNullString(orderObject.getString("id")),
                                                    resetNullString( orderObject.getString("user_id")),
                                                    resetNullString( orderObject.getString("order_id")),
                                                    resetNullString(orderObject.getString("order_date")),
                                                    resetNullString( orderObject.getString("product_id")),
                                                    resetNullString(orderObject.getString("product_name")),
                                                    orderObject.getInt("qty"),
                                                    orderObject.getInt("price"),
                                                    orderObject.getInt("total_price"),
                                                    resetNullString(orderObject.getString("payment_mode")),
                                                    resetNullString(orderObject.getString("status")),
                                                    resetNullString(orderObject.getString("image"))
                                            ));
                                        }
                                    }
                                    JSONArray plannJsonArray = object.getJSONArray("plans");
                                    userPlanLists = new ArrayList<>();
                                    String weight="";Integer price=0;


                                    for(int planList=0;planList<plannJsonArray.length();planList++)
                                    {
                                        JSONObject planObject = plannJsonArray.getJSONObject(planList);
                                        {
                                            if (planObject.getString("weight").equalsIgnoreCase("null")) {
                                                weight = "";
                                            } else {
                                                weight = planObject.getString("weight");
                                            }

                                            if (planObject.getString("price").equalsIgnoreCase("null")) {
                                                price = 0;
                                            } else {
                                                price = Integer.valueOf(planObject.getString("price"));
                                            }
                                            userPlanLists.add(new BeanUserPlanList(  resetNullString(planObject.getString("id")),
                                                    resetNullString( planObject.getString("product_name")),
                                                    weight,
                                                    price

                                            ));
                                        }
                                    }


                                    userLists.add(new BeanUserList(resetNullString(object.getString("id")),
                                            resetNullString( object.getString("name")),
                                            resetNullString( object.getString("mobile_no")),
                                            resetNullString(object.getString("email")),
                                            resetNullString( object.getString("flate_no")),
                                            resetNullString(object.getString("apartment")),
                                            resetNullString(object.getString("area")),
                                            resetNullString(object.getString("user_plan_id")),
                                            resetNullString(object.getString("morning")),
                                            resetNullString(object.getString("evening")),
                                            resetNullString(object.getString("milk_plan_id")),
                                            object.getString("milk_plan_name"),
                                            milk_weight,
                                            milk_price,
                                            resetNullString(object.getString("milk_shift")),
                                            resetNullString(object.getString("milk_status")),
                                            resetNullString(object.getString("milk_qty")),
                                            resetNullString(object.getString("milk_return_bottal")),
                                            extraOrderDetailsList,
                                            pendingBottle,
                                            userPlanLists


                                    ));

                                }


                            }
                            System.out.println("==userList=morning=="+userLists.size());

                            initRecycleView();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            serviceCaller.addNameValuePair("deliveryboy_id",StorePrefs.getDefaults(KEY_id,mContext));
            serviceCaller.execute(AppConstant.CustomerUserListApi);
        } else {
            showToast(mContext, mContext.getString(R.string.please_check_internet));
        }
    }

    private void initRecycleView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycle__userList.setLayoutManager(linearLayoutManager);
        userListApdater = new UserListApdater(mContext,userLists,"e");
        recycle__userList.setAdapter(userListApdater);
    }

}

