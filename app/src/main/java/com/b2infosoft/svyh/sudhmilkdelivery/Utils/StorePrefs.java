package com.b2infosoft.svyh.sudhmilkdelivery.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.b2infosoft.svyh.sudhmilkdelivery.Database.DatabaseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.resetNullString;


/**
 * Created by Choudhary on 09/02/2019.
 */

public class StorePrefs {
    public static final String KEY_LogegIn = "LogedIn", KEY_Auth_token = "token",
            KEY_id = "id", KEY_name = "name", KEY_email = "email",
            KEY_mobile_no = "mobile_no", KEY_address = "address",
            KEY_User_Image = "user_image",
            KEY_user_id = "user_id", KEY_status = "status",
            KEY_Notification = "notification_count",
            KEY_User_plan_id = "user_plan_id",
            KEY_USER_TOKEN = "remember_token";




    public static void createLoginSession(Context mContext, JSONObject jsonObject) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        try {

            editor.putString(KEY_id, resetNullString(jsonObject.getString(KEY_id)));
            editor.putString(KEY_name, resetNullString(jsonObject.getString(KEY_name)));
            editor.putString(KEY_email, resetNullString(jsonObject.getString(KEY_email)));
            editor.putString(KEY_mobile_no, resetNullString(jsonObject.getString(KEY_mobile_no)));
            editor.putString(KEY_address, resetNullString(jsonObject.getString(KEY_address)));
            editor.putString(KEY_User_Image, resetNullString(jsonObject.getString(KEY_User_Image)));
            //editor.putString(KEY_area, resetNullString(jsonObject.getString(KEY_area)));
            editor.putString(KEY_user_id, resetNullString(jsonObject.getString(KEY_user_id)));
            editor.putString(KEY_status, resetNullString(jsonObject.getString(KEY_status)));
        editor.putString(KEY_USER_TOKEN, resetNullString(jsonObject.getString(KEY_USER_TOKEN)));
            editor.putString(KEY_LogegIn, "yes");

            System.out.println("==editior==="+editor.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        editor.apply();
    }


    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getDefaults( String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static void setIntValueSession(String key, Integer value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static Integer getIntValueSesion( String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Integer value = preferences.getInt(key, 0);
        return value;
    }

    public static void clearDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key).apply();

        Log.d("CLEARED PREFERENCES : ", "KEY :=====> " + key);
    }

    public static void logoutDefaults(Context context) {
       DatabaseHandler db = new DatabaseHandler(context);
        db.deleteDatabase(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();

        Log.d("CLEARED PREFERENCES : ", "editor :=====> " + editor);
    }


}
