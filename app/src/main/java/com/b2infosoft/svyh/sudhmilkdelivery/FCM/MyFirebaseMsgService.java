package com.b2infosoft.svyh.sudhmilkdelivery.FCM;

/**
 * Created by Choudhary on 17-Jan-19.
 */

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.b2infosoft.svyh.sudhmilkdelivery.Activity.MainActivity;
import com.b2infosoft.svyh.sudhmilkdelivery.Activity.SplashActivity;
import com.b2infosoft.svyh.sudhmilkdelivery.Database.DatabaseHandler;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.b2infosoft.svyh.sudhmilkdelivery.FCM.Config.FCMCHANNEL_DESCRIPTION;
import static com.b2infosoft.svyh.sudhmilkdelivery.FCM.Config.FCMCHANNEL_NAME;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs.KEY_Notification;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.resetNullString;


public class MyFirebaseMsgService extends FirebaseMessagingService {
    private static final String TAG = " Shoukin Jaat== FCMService==";
    Map<String, String> data;
    DatabaseHandler db;
    String title = "", message = "", type = "", image = "";

    JSONObject json;
    int notif_count = 0;
    String refreshedToken = "";
    Context mContext;
    private int numMessages = 0;
    LocalBroadcastManager broadcaster;
    public static String REQUEST_ACCEPT = "notifction";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        mContext = this;

        db = new DatabaseHandler(mContext);
        System.out.println("remoteMessage==data=>>" + remoteMessage.getData().toString());
        try {
            json = new JSONObject();
            if (remoteMessage.getNotification() != null) {
                System.out.println(TAG + "====notification=body== " + remoteMessage.getNotification().getBody());
                data = new HashMap<String, String>();
                title = remoteMessage.getNotification().getTitle();
                message = resetNullString(remoteMessage.getNotification().getBody());
                System.out.println(TAG + "====notification=title== " + title);
                System.out.println(TAG + "====notification=message== " + message);

                if (message.length() > 0) {
                    createNotification(message, title, type);

                }

                json = new JSONObject(remoteMessage.getData().toString());
                System.out.println(TAG + "json==web== " + json.toString());
                if (isAppIsInBackground(mContext)) {
                    System.out.println(TAG + "app background== " + true);
                }
                JSONObject data = json.getJSONObject("data");
                if (data.has("image")) {
                    image = data.getString("image");
                }

                title = data.getString("title");
                message = data.getString("description");
                type = data.getString("type");

                createNotification(message, title, type);

            } else if (remoteMessage.getData().size() > 0) {

                title = remoteMessage.getData().get("title");
                message = resetNullString(remoteMessage.getData().get("description"));
                type = resetNullString(remoteMessage.getData().get("type"));
                createNotification(message, title, type);
            }

        } catch (Exception e)
        {
            System.out.println(TAG + "===Exception: =error==" + e.getMessage());
        }

    }


    private void createNotification(String messageBody, String title, String type)
    {
        int notificationID = new Random().nextInt(3000);
        if (StorePrefs.getIntValueSesion(KEY_Notification, this) == null ||
                StorePrefs.getIntValueSesion(KEY_Notification, this) == 0)
        {
            System.out.println(TAG + "===notif_count:==== " + notif_count);

            notif_count = 0;
        } else {
            notif_count = StorePrefs.getIntValueSesion(KEY_Notification, this);

        }
        ++notif_count;
        StorePrefs.setIntValueSession(KEY_Notification, notif_count, this);
        broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
        Intent msgintent = new Intent(REQUEST_ACCEPT);
        broadcaster.sendBroadcast(msgintent);
        try {
            if (messageBody == null || messageBody == "")
            {
                messageBody = "";
            }
            if (title == null || title == "")
            {
                title = "SudhMilk delivery";
            }
            db.addNotification(title, type, messageBody);
            Intent intent = null;
            if (type.equalsIgnoreCase("delete"))
            {
                StorePrefs.logoutDefaults(mContext);
                intent = new Intent(mContext, SplashActivity.class);
            }
            else
                {

                intent = new Intent(mContext, MainActivity.class);
            }
            intent.putExtra("message", messageBody);
            intent.putExtra("messageTitle", title);
            intent.putExtra("type", type);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultIntent = PendingIntent.getActivity(this,
                    0, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(mContext,
                            mContext.getResources().getString(R.string.notification_channel_id))
                            .setSmallIcon(R.drawable.ic_app_logo)
                            .setContentTitle(title)
                            .setContentText(messageBody)
                            .setAutoCancel(true)
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setContentIntent(resultIntent);
            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setupChannels(notificationManager);

            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setColor(getResources().getColor(R.color.colorPrimaryDark));
            }
            assert notificationManager != null;
            notificationManager.notify(notificationID, notificationBuilder.build());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(NotificationManager notificationManager) {

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(mContext.getString(R.string.notification_channel_id),
                FCMCHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(FCMCHANNEL_DESCRIPTION);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null)
        {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }
}