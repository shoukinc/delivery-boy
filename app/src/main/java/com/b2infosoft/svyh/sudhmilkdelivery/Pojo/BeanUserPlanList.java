package com.b2infosoft.svyh.sudhmilkdelivery.Pojo;

public class BeanUserPlanList
{
     public  String id = "";
    public  String product_name = "";
    public  String weight = "";
    public  Integer price = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }



    public BeanUserPlanList(String id, String product_name, String weight, Integer price) {
        this.id = id;
        this.product_name = product_name;
        this.weight = weight;
        this.price = price;
    }


}
