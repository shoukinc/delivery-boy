package com.b2infosoft.svyh.sudhmilkdelivery.Interface;


import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanNotification_Item;

public interface NotificationClickListner {
    void onAdapterClick(BeanNotification_Item notification_item);
}
