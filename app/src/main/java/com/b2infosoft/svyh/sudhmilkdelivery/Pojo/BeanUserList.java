package com.b2infosoft.svyh.sudhmilkdelivery.Pojo;


import java.util.List;

public class BeanUserList {

    public String user_id = "";
    public String user_name = "";
    public String user_mobile_no = "";
    public String user_email = "";
    public String user_flate_no = "";
    public String user_apartment = "";
    public String user_area = "";
    public String user_planId = "";
    public String shift_morning = "";
    public String shift_evening = "";
    public String milk_planId= "";
    public String milk_plan_name= "";
    public String milk_weight= "";
    public Integer milk_price= 0;
    public String milk_shift= "";
    public String milk_status_delivery= "";
    public String milk_qty= ""; public String milk_return_bottal= "";
    public List<BeanExtraOrderDetail> orderDetailList ;
    public String totalPendingBottle = "";
    public List<BeanUserPlanList> userPlanLists ;



    public BeanUserList(String user_id, String user_name, String user_mobile_no, String user_email, String user_flate_no, String user_apartment, String user_area, String user_planId, String shift_morning, String shift_evening, String milk_planId, String milk_plan_name, String milk_weight, Integer milk_price, String milk_shift, String milk_status_delivery, String milk_qty, String milk_return_bottal, List<BeanExtraOrderDetail> orderDetailList, String totalPendingBottle, List<BeanUserPlanList> userPlanLists) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_mobile_no = user_mobile_no;
        this.user_email = user_email;
        this.user_flate_no = user_flate_no;
        this.user_apartment = user_apartment;
        this.user_area = user_area;
        this.user_planId = user_planId;
        this.shift_morning = shift_morning;
        this.shift_evening = shift_evening;
        this.milk_planId = milk_planId;
        this.milk_plan_name = milk_plan_name;
        this.milk_weight = milk_weight;
        this.milk_price = milk_price;
        this.milk_shift = milk_shift;
        this.milk_status_delivery = milk_status_delivery;
        this.milk_qty = milk_qty;
        this.milk_return_bottal = milk_return_bottal;
        this.orderDetailList = orderDetailList;
        this.totalPendingBottle = totalPendingBottle;
        this.userPlanLists = userPlanLists;
    }


    public String getTotalPendingBottle() {
        return totalPendingBottle;
    }

    public void setTotalPendingBottle(String totalPendingBottle) {
        this.totalPendingBottle = totalPendingBottle;
    }





    public List<BeanUserPlanList> getUserPlanLists() {
        return userPlanLists;
    }

    public void setUserPlanLists(List<BeanUserPlanList> userPlanLists) {
        this.userPlanLists = userPlanLists;
    }

    public String getMilk_weight() {
        return milk_weight;
    }

    public void setMilk_weight(String milk_weight) {
        this.milk_weight = milk_weight;
    }

    public Integer getMilk_price() {
        return milk_price;
    }

    public void setMilk_price(Integer milk_price) {
        this.milk_price = milk_price;
    }




    public String getShift_evening() {
        return shift_evening;
    }

    public void setShift_evening(String shift_evening) {
        this.shift_evening = shift_evening;
    }

    public String getShift_morning() {
        return shift_morning;
    }

    public void setShift_morning(String shift_morning) {
        this.shift_morning = shift_morning;
    }

    public String getMilk_planId() {
        return milk_planId;
    }

    public void setMilk_planId(String milk_planId) {
        this.milk_planId = milk_planId;
    }

    public String getMilk_plan_name() {
        return milk_plan_name;
    }

    public void setMilk_plan_name(String milk_plan_name) {
        this.milk_plan_name = milk_plan_name;
    }

    public String getMilk_shift() {
        return milk_shift;
    }

    public void setMilk_shift(String milk_shift) {
        this.milk_shift = milk_shift;
    }

    public String getMilk_status_delivery() {
        return milk_status_delivery;
    }

    public void setMilk_status_delivery(String milk_status_delivery) {
        this.milk_status_delivery = milk_status_delivery;
    }

    public String getMilk_qty() {
        return milk_qty;
    }

    public void setMilk_qty(String milk_qty) {
        this.milk_qty = milk_qty;
    }

    public String getMilk_return_bottal() {
        return milk_return_bottal;
    }

    public void setMilk_return_bottal(String milk_return_bottal)
    {
        this.milk_return_bottal = milk_return_bottal;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_mobile_no() {
        return user_mobile_no;
    }

    public void setUser_mobile_no(String user_mobile_no) {
        this.user_mobile_no = user_mobile_no;
    }

    public String getUser_flate_no() {
        return user_flate_no;
    }

    public void setUser_flate_no(String user_flate_no) {
        this.user_flate_no = user_flate_no;
    }

    public String getUser_apartment() {
        return user_apartment;
    }

    public void setUser_apartment(String user_apartment) {
        this.user_apartment = user_apartment;
    }

    public String getUser_area() {
        return user_area;
    }

    public void setUser_area(String user_area) {
        this.user_area = user_area;
    }

    public String getUser_planId() {
        return user_planId;
    }

    public void setUser_planId(String user_planId) {
        this.user_planId = user_planId;
    }





    public List<BeanExtraOrderDetail> getOrderDetailList() {
        return orderDetailList;
    }

    public void setOrderDetailList(List<BeanExtraOrderDetail> orderDetailList) {
        this.orderDetailList = orderDetailList;
    }
}

