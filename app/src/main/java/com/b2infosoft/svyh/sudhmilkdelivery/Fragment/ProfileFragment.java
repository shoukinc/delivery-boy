package com.b2infosoft.svyh.sudhmilkdelivery.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.b2infosoft.svyh.sudhmilkdelivery.Activity.LoginActivity;
import com.b2infosoft.svyh.sudhmilkdelivery.Activity.MainActivity;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;
import com.bumptech.glide.Glide;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs.logoutDefaults;

public class ProfileFragment extends Fragment
{
    View view;
    Toolbar toolbar;

   LinearLayout details_layout;
    Context mContext;
    TextView tv_name,tv_address,tv_mobile,tv_email;
    Animation slide;
    ImageView img_profile;
    Button btn_logout;



    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        mContext = getActivity();
        toolbar = view.findViewById(R.id.tool_bar);
        toolbar.setTitle(mContext.getString(R.string.title_profile));
        img_profile = view.findViewById(R.id.img_profile);
        tv_name = view.findViewById(R.id.tv_name);
        tv_address = view.findViewById(R.id.tv_address);
        details_layout = view.findViewById(R.id.detailLayout);
        tv_mobile = view.findViewById(R.id.tv_mobile);
        tv_email = view.findViewById(R.id.tv_email);
        btn_logout = view.findViewById(R.id.btn_logout);


        slide = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
        details_layout.startAnimation(slide);


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
            }
        });



        tv_name.setText("  "+StorePrefs.getDefaults(StorePrefs.KEY_name,mContext));
        tv_address.setText("  "+StorePrefs.getDefaults(StorePrefs.KEY_address,mContext));
        tv_mobile.setText("  "+StorePrefs.getDefaults(StorePrefs.KEY_mobile_no,mContext));
        tv_email.setText("  "+StorePrefs.getDefaults(StorePrefs.KEY_email,mContext));

        Glide.with(mContext).load(R.drawable.ic_app_logo).into(img_profile);




        return view;
    }
    public void Logout() {
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                StorePrefs.logoutDefaults(getActivity());
                                Intent intent = new Intent(mContext, LoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();

                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }


}
