package com.b2infosoft.svyh.sudhmilkdelivery.Interface;


import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUploadPlanList;

import java.util.List;

public interface CalculatePriceListener {
    void CalculatetotalPrice(int totalPrice, int totalWeight,
                             List<BeanUploadPlanList> mList, int bottle);
}
