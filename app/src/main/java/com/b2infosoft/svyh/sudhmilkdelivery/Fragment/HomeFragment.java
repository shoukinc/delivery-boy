package com.b2infosoft.svyh.sudhmilkdelivery.Fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.b2infosoft.svyh.sudhmilkdelivery.Adapter.TabLayoutListAdapter;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.google.android.material.tabs.TabLayout;

public class HomeFragment extends Fragment
{
    View view;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    Context mContext;
    TabLayoutListAdapter tabLayoutListAdapter;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        mContext = getActivity();
        toolbar = view.findViewById(R.id.tool_bar);
      //  toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
       toolbar.setTitle(mContext.getString(R.string.title_home));
        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.morning)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.evening)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
         viewPager =view.findViewById(R.id.view_pager);
        tabLayoutListAdapter = new TabLayoutListAdapter(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabLayoutListAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });

        return view;
    }
}
