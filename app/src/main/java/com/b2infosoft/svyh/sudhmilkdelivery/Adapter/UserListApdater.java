package com.b2infosoft.svyh.sudhmilkdelivery.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.b2infosoft.svyh.sudhmilkdelivery.Interface.CalculatePriceListener;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanExtraOrderDetail;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUploadPlanList;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUserList;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanUserPlanList;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;
import com.b2infosoft.svyh.sudhmilkdelivery.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.AppConstant.AddMilkEntryApi;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.getSimpleDate;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.showToast;

public class UserListApdater extends RecyclerView.Adapter<UserListApdater.MyViewHolder> implements CalculatePriceListener
{
    Context mContext;
    List<BeanUserList> userLists;
    List<BeanUserList> mListFilter;
    int qtncount = 0;
    Dialog dialog;
    TextView tvPendingBottle, tvBottleCount, tv_shift, tv_milk_price, tvPlanList;
    Button dialogButtonclose;
    RecyclerView recycle_extra_order, recycle_plan_list;
    String strTotalWeight = "";
    String strTotalPrice = "";
    Button btn_save;
    LinearLayout extraOrderLaoyut;
    String Milkshift = "", user_planId = "", plan_name = "", qty = "", strTotalBottle = "", user_id = "", delivery_status = "", product_name = "";
    String date = "",pendingBottle="";
    List<BeanExtraOrderDetail> extraOrderDetailList;
    List<BeanUserPlanList> userPlanLists;
    Integer Itenposition = 0;
    Toolbar toolbar;
    JSONArray jsonPlanArray;
    LinearLayout deliveredLayout;
    RelativeLayout relativeLayout;

    public UserListApdater(Context mContext, List<BeanUserList> userLists, String milkshift)
    {
        this.mContext = mContext;
        this.Milkshift = milkshift;
        this.userLists = userLists;
        mListFilter = new ArrayList<>();
        this.mListFilter.addAll(userLists);
        jsonPlanArray=new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.customer_row_item, viewGroup, false);
        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        final BeanUserList userAlbum = userLists.get(position);

        String user_name = userAlbum.getUser_name();
        String first_wordC_name = user_name.substring(0, 1).toUpperCase() + user_name.substring(1);
        myViewHolder.textView_userName.setText(first_wordC_name);

        myViewHolder.tv_mobile.setText(userAlbum.getUser_mobile_no());
        myViewHolder.tv_UserId.setText(userAlbum.getUser_id());

        delivery_status = userAlbum.getMilk_status_delivery();
        if (delivery_status.equalsIgnoreCase("1")) {
            myViewHolder.textView_status.setText("Delivered");
            myViewHolder.textView_status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checked, 0, 0, 0);

        } else if (delivery_status.equalsIgnoreCase("0")) {
            myViewHolder.textView_status.setText("Vaccation");
            Drawable img = mContext.getResources().getDrawable(R.drawable.ic_cancel);
            img.setBounds(0, 10, 40, 50);
            myViewHolder.textView_status.setCompoundDrawables(img, null, null, null);
        } else if (delivery_status.equalsIgnoreCase("null")) {
            myViewHolder.textView_status.setText("Pending");
            myViewHolder.textView_status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_icon, 0, 0, 0);

            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                public void onClick(View v) {

                    userPlanLists = userLists.get(position).getUserPlanLists();

                    user_planId = userAlbum.getUser_planId();
                    pendingBottle = userAlbum.getTotalPendingBottle();
                    user_id = userAlbum.getUser_id();
                    date = getSimpleDate();
                    Itenposition = position;
                    extraOrderDetailList = new ArrayList<>();
                    qty = pendingBottle;
                    if (!userAlbum.getOrderDetailList().isEmpty()) {
                        extraOrderDetailList.addAll(userAlbum.getOrderDetailList());
                    }

                    viewDailog();
                }
            });
        }

    }


    public int getItemCount() {
        return userLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_userName, textView_status, tv_mobile, tv_UserId;
        ImageView imageView_user;
        CardView customer_row;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_userName = itemView.findViewById(R.id.tv_userName);
            textView_status = itemView.findViewById(R.id.tvStatus);
            tv_mobile = itemView.findViewById(R.id.tv_Mobile);
            tv_UserId = itemView.findViewById(R.id.tv_UserId);

            customer_row = itemView.findViewById(R.id.customer_row);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void viewDailog() {

        dialog = new Dialog(mContext);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_product_add_entry);

        dialogButtonclose = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        tvPendingBottle = dialog.findViewById(R.id.tv_pending_bottle);

        tv_milk_price = dialog.findViewById(R.id.tv_milk_price);
        tv_shift = dialog.findViewById(R.id.tv_shift);

        extraOrderLaoyut = dialog.findViewById(R.id.extraOrderLaoyut);
        deliveredLayout = dialog.findViewById(R.id.deliveredLayout);
        relativeLayout = dialog.findViewById(R.id.relativeLayout);

        toolbar =dialog.findViewById(R.id.tool_bar);
        //  toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle(mContext.getString(R.string.plan_details));

        recycle_extra_order = dialog.findViewById(R.id.recycle_extra_order);
        ExtraOrderListAdapter adapter = new ExtraOrderListAdapter(mContext, extraOrderDetailList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycle_extra_order.setLayoutManager(linearLayoutManager);
        recycle_extra_order.setAdapter(adapter);

        recycle_plan_list = dialog.findViewById(R.id.recycle_plan_list);
        PlanListAdapter planListAdpter = new PlanListAdapter(mContext, userPlanLists, this);
        LinearLayoutManager linearLayout = new LinearLayoutManager(mContext);
        recycle_plan_list.setLayoutManager(linearLayout);
        recycle_plan_list.setAdapter(planListAdpter);

        if (!extraOrderDetailList.isEmpty()) {
            extraOrderLaoyut.setVisibility(View.VISIBLE);
        } else {
            extraOrderLaoyut.setVisibility(View.GONE);
        }

        if (Milkshift.equalsIgnoreCase("m"))
        {
            tv_shift.setText("Morning");
        } else {
            tv_shift.setText("Evening");
        }

        dialogButtonclose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.cancel_entry))
                        .setMessage(mContext.getString(R.string.are_you_sure_want_cancel_entry))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })

                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                dialog.dismiss();

            }
        });
        btn_save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                validation();
            }
        });

        tvPendingBottle.setText(pendingBottle);

        dialog.show();
    }

    public void CalculatetotalPrice(int totalPrice, int totalweight, List<BeanUploadPlanList> uploadPlanList, int totalbottle)
    {
        strTotalPrice=Integer.toString(totalPrice);
        strTotalWeight=Integer.toString(totalweight);
        strTotalBottle = String.valueOf(totalbottle);

        tv_milk_price.setText(mContext.getString(R.string.Rupee_symbol)+" "+strTotalPrice);
         jsonPlanArray=new JSONArray();
         System.out.println("==uploadPlanList==="+uploadPlanList.size());

        for(int i=0; i<uploadPlanList.size();i++)
        {
            try
            {
                if(!uploadPlanList.get(i).getWeight().equalsIgnoreCase("0")) {
                    JSONObject planObject = new JSONObject();
                    planObject.put("plan_id", uploadPlanList.get(i).getId());
                    planObject.put("plan_name", uploadPlanList.get(i).getProduct_name());
                    planObject.put("weight", uploadPlanList.get(i).getWeight());
                    planObject.put("price", uploadPlanList.get(i).getPrice());
                    planObject.put("return_bottal", uploadPlanList.get(i).returnBottle);
                    jsonPlanArray.put(planObject);
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        System.out.println("==jsonPlanArray==="+jsonPlanArray.toString());

    }



    public void validation() {

        if (Milkshift.length() == 0) {

            tv_shift.requestFocus();
            showToast(mContext, mContext.getResources().getString(R.string.shift_required));
        } else {
            tv_shift.clearFocus();
        }




        addMilkData();

    }

    public void addMilkData() {
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) throws JSONException {
                try {

                    Log.d("==response==", response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        if(dialog!=null){
                            dialog.dismiss();}

                        showToast(mContext, jsonObject.getString("message"));
                        userLists.get(Itenposition).setMilk_status_delivery("1");
                        notifyDataSetChanged();

                    } else {
                        showToast(mContext, jsonObject.getString("message"));
                    }

                } catch (Exception e)
                { e.printStackTrace(); }

            }
        };
        System.out.println("===milk_entry===="+jsonPlanArray);
        webServiceCaller.addNameValuePair("user_id", user_id);
        webServiceCaller.addNameValuePair("deliveryboy_id", StorePrefs.getDefaults(StorePrefs.KEY_id, mContext));
         webServiceCaller.addNameValuePair("entry_date", date);
         webServiceCaller.addNameValuePair("milk_entry", String.valueOf(jsonPlanArray));

       /* StringEntity se = null;
        try {
            se = new StringEntity(jsonOrder.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        webServiceCaller.execute(AddMilkEntryApi);


    }

    public void filterSearch(String charText) {
        Log.d("==search filter=======", charText);
        charText = charText.toLowerCase(Locale.getDefault());
        userLists.clear();
        if (charText.length() == 0) {
            userLists.addAll(mListFilter);
        } else {
            for (BeanUserList wp : mListFilter) {
                if (wp.getUser_mobile_no().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userLists.add(wp);
                } else if (wp.getUser_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userLists.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


}
