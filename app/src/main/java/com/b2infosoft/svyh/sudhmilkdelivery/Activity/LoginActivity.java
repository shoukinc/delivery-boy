package com.b2infosoft.svyh.sudhmilkdelivery.Activity;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;
import com.b2infosoft.svyh.sudhmilkdelivery.webservice.WebServiceCaller;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.AppConstant.LoginApi;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs.KEY_USER_TOKEN;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.PERMISSIONS;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.PERMISSION_ALL;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.hasPermissions;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.isNetworkAvaliable;
import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.UtilityMethod.showAlertBox;

public class LoginActivity extends AppCompatActivity {

    Button btn_login;
    ProgressBar progressBar;
    EditText et_mobileNo,et_password;
    String mobile = "";
    String password = "";
    String firebase_token = "";
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btn_login = findViewById(R.id.login_button);
        progressBar = findViewById(R.id.progressBar);
        et_mobileNo = findViewById(R.id.ediMobile);
        et_password = findViewById(R.id.ediPassword);
        mContext = LoginActivity.this;

        FirebaseApp.initializeApp(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

       btn_login.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mobile = et_mobileNo.getText().toString().trim();
               password = et_password.getText().toString().trim();

               if (mobile.length() == 0) {
                   showAlertBox(mContext, mContext.getString(R.string.Mobile_no_required));
               }
               else if(mobile.length()<10)
               {
                   showAlertBox(mContext,  mContext.getString(R.string.not_valid_number));
               }
               else if (password.length() == 0) {
                   showAlertBox(mContext, mContext.getString(R.string.password_required));
               } else {

                   if (isNetworkAvaliable(mContext)) {
                       progressBar.setVisibility(View.VISIBLE);
                       btn_login.setVisibility(View.GONE);

                       userLogin();
                   } else {
                       Toast.makeText(mContext, mContext.getString(R.string.please_check_internet),Toast.LENGTH_LONG).show();
                   }
               }
           }

       });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void userLogin() {
       firebase_token = FirebaseInstanceId.getInstance().getToken();

        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK_Log, LoginActivity.this, "Logging In...", true) {
            @Override
            public void handleResponse(String response) throws JSONException {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("===response==="+response);
                    if (jsonObject.getBoolean("success")) {
                        Toast.makeText(mContext, jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                        JSONObject jsonData = jsonObject.getJSONObject("data");
                      //  StorePrefs.setDefaults(KEY_Auth_token, jsonData.getString(KEY_Auth_token), mContext);
                        //StorePrefs.createLoginSession(mContext, jsonData.getJSONObject("data"));
                        JSONObject jsonObject1 = jsonData.getJSONObject("data");
                        StorePrefs.setDefaults(KEY_USER_TOKEN,jsonObject1.getString(KEY_USER_TOKEN),mContext);
                        StorePrefs.createLoginSession(mContext, jsonData.getJSONObject("data"));


                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    } else {
                        Toast.makeText(mContext, jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    }
                    progressBar.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);

                    e.printStackTrace();
                }

            }
        };
        webServiceCaller.addNameValuePair("mobile_no", mobile);
        webServiceCaller.addNameValuePair("password", password);
        webServiceCaller.addNameValuePair("firebase_token", firebase_token);
        webServiceCaller.execute(LoginApi);

    }









}
