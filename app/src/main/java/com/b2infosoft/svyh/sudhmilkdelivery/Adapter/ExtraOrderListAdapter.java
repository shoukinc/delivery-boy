package com.b2infosoft.svyh.sudhmilkdelivery.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanExtraOrderDetail;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.AppConstant.BaseImageUrl;



public class ExtraOrderListAdapter extends RecyclerView.Adapter<ExtraOrderListAdapter.MyViewHolder>
{
    List<BeanExtraOrderDetail> extraOrderDetailList;
    Context mContext;

    public ExtraOrderListAdapter(Context mContext, List<BeanExtraOrderDetail> extraOrderDetailLis)
    {
      this.mContext = mContext;
      this.extraOrderDetailList=extraOrderDetailLis;
    }

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.extra_product_order_row_item, viewGroup, false);
        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position)
    {

        final BeanExtraOrderDetail userAlbum = extraOrderDetailList.get(position);
       String  url = BaseImageUrl + userAlbum.getImage();
        Glide.with(mContext)
                .load(url)
                .apply(new RequestOptions().override(50, 50))
                .thumbnail(Glide.with(mContext).load(R.drawable.preloader))
                .into(myViewHolder.product_image);


        myViewHolder.tv_product_name.setText(userAlbum.getProduct_name());
        myViewHolder.tv_product_qty.setText(Integer.toString(userAlbum.getQty()));
        myViewHolder.tv_product_price.setText(mContext.getString(R.string.Rupee_symbol)+" "+Integer.toString(userAlbum.getTotal_price()));

    }

    @Override
    public int getItemCount()
    {
        return extraOrderDetailList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        ImageView product_image;
        TextView tv_product_name,tv_product_qty,tv_product_price;
        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            product_image = itemView.findViewById(R.id.image);
            tv_product_name = itemView.findViewById(R.id.tvTitle);
            tv_product_qty = itemView.findViewById(R.id.tvQty);
            tv_product_price = itemView.findViewById(R.id.tvPrice);


        }
    }



}
