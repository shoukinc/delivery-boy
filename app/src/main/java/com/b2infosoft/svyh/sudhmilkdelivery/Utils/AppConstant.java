package com.b2infosoft.svyh.sudhmilkdelivery.Utils;

public class AppConstant {


    public static final String BaseUrl = "http://milk.itli.in/";
    public static String BaseImageUrl = BaseUrl + "public/images/";
    public static final String user_status1 = "1";
    public static final String statusSuccess = "success";
    public static final String GetProductPlanAPi = BaseUrl + "api/get-product-plan";
    public static final String RegisterAPi = BaseUrl + "api/register";
    public static final String PostLoginAPi = BaseUrl + "api/login";
    public static final String ForgotPasswordAPi = BaseUrl + "api/forgot-request";
    public static final String LoginApi = BaseUrl + "api/deliveryboy-login?";
    public static final String CustomerUserListApi = BaseUrl + "api/get-all-user-list?";
    public static final String AddMilkEntryApi = BaseUrl + "api/add-milk-entry";


}
