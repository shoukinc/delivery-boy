package com.b2infosoft.svyh.sudhmilkdelivery.Fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.b2infosoft.svyh.sudhmilkdelivery.Adapter.Notification_item_adapter;
import com.b2infosoft.svyh.sudhmilkdelivery.Database.DatabaseHandler;
import com.b2infosoft.svyh.sudhmilkdelivery.Interface.NotificationClickListner;
import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanNotification_Item;
import com.b2infosoft.svyh.sudhmilkdelivery.R;
import com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs;

import java.util.ArrayList;
import java.util.Collections;

import static com.b2infosoft.svyh.sudhmilkdelivery.Utils.StorePrefs.KEY_Notification;

public class NotificationFragment extends Fragment implements NotificationClickListner
{
    View view;
    Toolbar toolbar;
    Context mContext;
    Notification_item_adapter adapter;
    ArrayList<BeanNotification_Item> mList;
    RecyclerView recyclerView;
    DatabaseHandler db;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_notification, container, false);

        mContext = getActivity();
        toolbar = view.findViewById(R.id.tool_bar);
       toolbar.setTitle(mContext.getString(R.string.title_notifications));
        recyclerView = view.findViewById(R.id.recyclerView);

        db = new DatabaseHandler(mContext);
        mList = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle = getArguments();

        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        mList = new ArrayList<>();
        mList = db.getNotificationList();
        Collections.reverse(mList);
        adapter = new Notification_item_adapter(mContext, mList,  this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        StorePrefs.setIntValueSession(KEY_Notification, 0, mContext);

    }


    public void onAdapterClick(BeanNotification_Item notification_item) {


    }
}
