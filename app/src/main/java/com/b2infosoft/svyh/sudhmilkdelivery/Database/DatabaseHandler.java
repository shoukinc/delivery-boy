package com.b2infosoft.svyh.sudhmilkdelivery.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.b2infosoft.svyh.sudhmilkdelivery.Pojo.BeanNotification_Item;

import java.util.ArrayList;


/**
 * Created by Choudhary on 22-Jun-19.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String KEY_Image = "image_url";

    //-----------Product Table Column names
    public static final String KEY_Status = "status";

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "SudhMilkDelivery";
    //--------------Table name-----------------

    private static final String Table_Notification = "table_notification";
    //--------------Table Key Name-----------------
    private static final String KEY_Id = "id";
    private static final String KEY_Title = "title";
    private static final String KEY_Type = "type";
    private static final String KEY_Description = "description";




    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {




        String createNotificationTable = "CREATE TABLE " + Table_Notification + "("
                + KEY_Id + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_Title + " TEXT,"
                + KEY_Type + " TEXT,"
                + KEY_Description + " TEXT" + ")";


        db.execSQL(createNotificationTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Table_Notification);
        onCreate(db);
    }


       //----------------------------Add Notification-------------------------

    public void addNotification(String title, String type, String description) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_Title, title);
        values.put(KEY_Type, type);
        values.put(KEY_Description, description);
        // Inserting Row
        db.insert(Table_Notification, null, values);
        db.close(); // Closing database connection
    }

    //----------------------------Getting Notification-------------------------
    public ArrayList<BeanNotification_Item> getNotificationList() {
        ArrayList<BeanNotification_Item> mList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Table_Notification;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {

            do {
                mList.add(new BeanNotification_Item(
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_Id)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_Title)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_Type)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_Description))));
            } while (cursor.moveToNext());

        }

        db.close();
        return mList;
    }

    public void deleteNotification(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseHandler.Table_Notification, DatabaseHandler.KEY_Id + "=?", new String[]{id});
        db.close();
    }


    public void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }
}
